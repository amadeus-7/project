# coding: utf8
import project

import os
import tkinter as tk
import webbrowser

application_window = tk.Tk()
application_window.title("AI Based automation for PO Ingestion")

question_frame = tk.Frame(application_window).grid()
browse_frame = tk.Frame(application_window).grid()
answer_frame = tk.Frame(application_window).grid()

application_window.configure(background="#c7d5e8")
application_window.iconbitmap(r'icon.ico')
v = tk.IntVar()
def openFile():
    webbrowser.open_new(r"Output")

def callback():
    webbrowser.open_new(r"http://localhost/phpmyadmin/db_structure.php?server=1&db=project")
def print_label(file_name,r):
    tk.Label(answer_frame, height=2, text="                                                                                                                            ", background="#c7d5e8").grid(row=4,columnspan=2)
    tk.Label(answer_frame,text=file_name, font=("Corbel", 12), background="#c7d5e8",).grid(row=r, column=0,sticky='W')
    tk.Label(answer_frame, text="\t...Completed", font=("Corbel", 12), background="#c7d5e8").grid(row=r, column=1,sticky='W')


def complete(r):
    tk.Label(browse_frame, width=30, height=2, font=("Comic Sans MS", 12), text="\nFile Converted Successfully...",
             background="#c7d5e8", borderwidth=0).grid(row=r + 1, columnspan=2)
    tk.Button(browse_frame, width=30, height=1, font=("Comic Sans MS", 12, 'underline'),
              text="Click to Check Output Directory", background="#c7d5e8", borderwidth=0, command=openFile).grid(
        row=r + 2, columnspan=2)
    tk.Button(browse_frame, text="View Database", font=("Comic Sans MS", 12, 'underline'), background="#c7d5e8",
              borderwidth=0, command=callback).grid(row=r + 3, columnspan=2)
    tk.Button(browse_frame, text="View Graph", font=("Comic Sans MS", 12, 'underline'), background="#c7d5e8",
              borderwidth=0, command=call).grid(row=r + 4, columnspan=2)

def complete1(r):
    tk.Label(browse_frame, width=30, height=2, font=("Comic Sans MS", 12), text="\nFile Converted Successfully...",
             background="#c7d5e8", borderwidth=0).grid(row=r + 1, columnspan=2)
    tk.Button(browse_frame, width=30, height=1, font=("Comic Sans MS", 12, 'underline'),
              text="Click to Check Output Directory", background="#c7d5e8", borderwidth=0, command=openFile).grid(
        row=r + 2, columnspan=2)
    tk.Button(browse_frame, text="View Database", font=("Comic Sans MS", 12, 'underline'), background="#c7d5e8",
              borderwidth=0, command=callback).grid(row=r + 3, columnspan=2)

def call():
    project.graph()
def file_dialog():
    if v.get() == 1:
        for label in application_window.grid_slaves():
            if int(label.grid_info()["row"]) > 4:
                label.grid_forget()
        tk.Label(browse_frame,height=2,text="                                                                                                                            " ,font=("Gabriola", 12), background="#c7d5e8").grid(row=4,columnspan=2)

        tk.Label(browse_frame,height=2, text="Please Wait...",font=("Gabriola", 12), background="#c7d5e8").grid(row=4,columnspan=2)

        answer = tk.filedialog.askopenfilename(parent=application_window,
                                               initialdir=os.getcwd(),
                                               title="Please select a file:",
                                               filetypes=[('PDF files',
                                                           '*.pdf')])


        project.main2(answer)

    elif v.get() == 2:
        for label in application_window.grid_slaves():
            if int(label.grid_info()["row"]) > 4:
                label.grid_forget()
        tk.Label(browse_frame,height=2,text="                                                                                                                            " ,font=("Gabriola", 12), background="#c7d5e8").grid(row=4,columnspan=2)
        tk.Label(browse_frame,height=2, text="Please Wait...",font=("Gabriola", 12), background="#c7d5e8").grid(row=4,columnspan=4)

        answer = tk.filedialog.askdirectory(parent=application_window,
                                            initialdir=os.getcwd(),
                                            title="Please select a folder:")


        project.main1(answer)
    elif v.get() == 0:
        tk.Label(browse_frame,height=2, text="Please Choose any option",font=("Gabriola", 12),foreground="red", background="#c7d5e8").grid(row=4,columnspan=2)

tk.Label(question_frame,  width=60, height=2,font=("Comic Sans MS", 14),text="Please choose between File or Folder:", background="#c7d5e8").grid(row=0, columnspan=2)
tk.Radiobutton(question_frame, text="File",font=("Gabriola", 12),background="#c7d5e8",variable=v, value=1).\
    grid(row=1, sticky='W', in_=question_frame)
tk.Radiobutton(question_frame, text="Folder",font=("Gabriola", 12),background="#c7d5e8" ,variable=v, value=2).\
    grid(row=2, sticky='W', in_=question_frame)
tk.Button(browse_frame, text="Browse",font=("Comic Sans MS", 12),background="black",foreground="white",borderwidth=5, command=file_dialog).\
    grid(row=3, in_=browse_frame,columnspan=2)
tk.Label(browse_frame, height=2, text="", font=("Gabriola", 12), background="#c7d5e8").grid(row=4, columnspan=2)
