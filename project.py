# coding: utf8

import glob
from PyPDF2 import PdfFileReader, PdfFileWriter

import tabula
import os
import nltk
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import gui
from nltk.corpus import stopwords
import pandas as pd
import csv
import mysql.connector
import re
import numpy as np

mydb = mysql.connector.connect(host='localhost',
    user='root',
    passwd='',
    db='project')
cursor = mydb.cursor()

os.environ['JAVAHOME']="C:/Program Files/Java/jdk1.8.0_201/bin/java.exe"
final_item = []
f = []
final_qty = []
t = []
final_total = []

def main1(in_dir1):
    """
    This function takes the path of the folder containing the PDFs and returns
    a list of mapped word to position(top,left,width,height).
    """
    in_dir = in_dir1
    i = 5
    for file in glob.iglob(in_dir + '/*.pdf'):
        #split_pdf(file)
        js = pdf_to_json(file)
        text_list, area_list = json_to_list(js)

        top, left, right = list_ner(text_list, area_list)
        p=po_ner(text_list)
        bottom = list_in_out_ner(text_list, area_list)
        if bottom != -1:
            table_area(file, top, left, bottom, right,p)
        else:
            pass  # kept pass to implement logic in future extension

        gui.print_label(file, i)
        i = i + 1

    gui.complete(i)

def main2(in_dir1):
    """
    This function takes the path of the folder containing the PDFs and returns
    a list of mapped word to position(top,left,width,height).
    """

    file1 = in_dir1
    #split_pdf(file1)
    js = pdf_to_json(file1)
    text_list, area_list = json_to_list(js)

    top, left, right = list_ner(text_list, area_list)
    p=po_ner(text_list)
    bottom = list_in_out_ner(text_list, area_list)
    if bottom != -1:
        table_area(file1, top, left, bottom, right,p)
    else:
        pass  # kept pass to implement logic in future extension
    gui.print_label(file1,5)

    gui.complete1(6)
#
# def split_pdf(file):
#     """
#     function to read and split pdf pages
#     """
#     file_name = os.path.splitext(os.path.basename(file))[0]
#     pdf = PdfFileReader(file)
#     size = pdf.getNumPages()
#     print("There are {} pages in pdf".format(size))
#     for page in range(size):
#         pdf_writer = PdfFileWriter()
#         pdf_writer.addPage(pdf.getPage(page))
#         output_filename = '{}_page_{}.pdf'.format(file_name, page+1)
#         with open(output_filename, 'wb') as data:
#             pdf_writer.write(data)
#         print('Generated Pdf: {}'.format(output_filename))


def pdf_to_json(path):
    """
    This function takes the path of the folder containing the PDFs and converts
    it into JSON format.
    :param path: path to the PDF
    :return: JSON of the entire page
    """

    return tabula.read_pdf(path, output_format="json",
                           relative_area=True,
                           pages="all",
                           guess=False,
                           stream=True)


def json_to_list(data):
    """
    This function parses the json and returns a list of mapped word to
    position(top,left,width,height).
    :param data: JSON file
    :return: list of tuples (word,position)
    """
    print(data)
    word_list = []
    dim_list = []
    en_stops = set(stopwords.words('english'))
    for page in data:
        for det in page["data"]:
            for box in det:
                text = box["text"].split(" ")
                temp = [box["top"], box["left"], box["width"],
                        box["height"]]
                for word in text:
                    if word != "":
                        if word not in en_stops:
                            word_list.append(word)
                            dim_list.append(temp)
    print(word_list, dim_list)
    return word_list, dim_list


def list_ner(text_list, area_list):
    """
    This function runs the NER model on the list.
    :param area_list: list of tuples (position)
    :param text_list: list of tuples (word)
    :return: top, left and right of the table along 
             with the text list
    """
    # top, left, right = 0, 0, 0
    jar = 'lib/stanford-ner.jar'
    ner_model = 'lib/ner-model.ser.gz'
    ner_tagger = nltk.StanfordNERTagger(ner_model, jar, encoding='utf8')
    header_list = []
    tag_list = ner_tagger.tag(text_list)
    flag = 0
    for tag in tag_list:
        if tag[1] == "HEADER":
            header_list.append(area_list[flag])

        else:
            pass
        flag += 1
    top, left, right = header_list[0][0], 0.00, 612.00

    return top, left, right


def po_ner(text):
    """
    This function will returns the purchase order number/s.
    :param text: input text data to apply Rule base matching
    :return: Purchase Order Number
    """

    flag_po = 0
    flag_po_2 = 0
    token_list = []
    for tokens in text:
        tokens = re.sub(r'[.,:()"{}!@#$%^&*+~`]+', '', str(tokens))
        if flag_po_2 == 1 \
                and (str.isspace(tokens) or str.isalpha(tokens) or not tokens):
            flag_po_2 += 0
        elif flag_po_2 == 1:
            token_list.append(tokens)
            flag_po, flag_po_2 = 0, 0
        else:
            flag_po_2 = 0
        if tokens == "Purchase" \
                or tokens == "Reference" \
                or tokens == "PO" \
                or tokens == "Our":
            flag_po = 1
        elif (tokens == "Order"
                or tokens == "number"
                or tokens == "No")\
                and flag_po == 1:
            flag_po_2 = 1
        else:
            pass
    print("Purchase Order", token_list[0])
    return token_list[0]


def list_in_out_ner(text_list, area_list):
    """
    This function runs the NER model on the list.
    :param area_list: list of tuples (position)
    :param text_list: list of tuples (word)
    :return: bottom of the table
    """
    jar = 'lib/stanford-ner.jar'
    data_model = 'lib/ner_in_out-model.ser.gz'
    ner_tagger = nltk.StanfordNERTagger(data_model, jar, encoding='utf8')
    table_data = []
    outliers = []
    tag_list = ner_tagger.tag(text_list)
    flag = 0
    content_flag = 0
    for tag in tag_list:
        
        if tag[1] == "IN":
            table_data.append(area_list[flag])
            content_flag = 1

        elif tag[1] == "OUT":
            outliers.append(area_list[flag])
        else:
            pass
        flag += 1
    if content_flag != 0:
        bottom = table_data[-1][0] + table_data[-1][3]
    else:
        bottom = -1

    return bottom

def table_area(path, top, left, bottom, right,p):
    """
    Extract the table contents based on the area provided to the function
    :param path: path of the PDF
    :param top: top of the table
    :param left: left of the table
    :param bottom: bottom of the table
    :param right: right of the table
    :return: dataframe symbolizing the table
    """
    global final_qty
    global final_item
    global f
    # global t
    global final_total
    out_dir = "Output"
    df = tabula.read_pdf(path, output_format="dataframe",
                         area=[top, left, bottom, right],
                         pages="all",
                         guess=False,
                         stream=True)


    df['PO Number'] = ''
    df['PO Number'][1] = p

    _, filename = os.path.split(path)

    table_name = os.path.splitext(filename)[0]
    a=str.maketrans('- /:.#','______')
    table_name=table_name.translate(a)


    filename = filename.replace(".pdf", ".csv")
    #df = df.assign(PO_NUM=pd.Series([p for _ in range(len(df.index))]).values)

    header_list = df.head(0)
    data_list = df.iloc[0]
    flag = 0
    nan_flag = 0
    title_list = []

    for head in header_list:

        if(filename=="00353_page_1.csv" or filename=="HP9D-39006_page_1.csv"):
            if(str(data_list[flag])=="nan"):
               s = str(data_list[flag + 1])
               data_list[flag]=s


        if (head == ("Unnamed: " + str(flag)))\
                and (str(data_list[flag]) is not "nan"):
            title_list.append(head.replace(head, str(data_list[flag])))
            nan_flag += 1

        elif nan_flag > 0 and head != "PO Number":
            if str(data_list[flag]) != "nan":
                title_list.append(head + " " + str(data_list[flag]))
            else:
                title_list.append(head)
        elif nan_flag == 0 and str(data_list[flag]) != "nan" \
                and head != "PO Number":
            title_list.append(head + " " + str(data_list[flag]))
        else:
            title_list.append(head)
        flag += 1


    data_list = list(data_list)
    df.columns = title_list

    for i in df.columns:

        if (i=="Net Unit Unit Price"):
            df.rename(columns={"Net Unit Unit Price": "Net Unit"}, inplace=True)



    lst=df.columns.tolist()
    
    for i in lst:
        if(i=="Item No."):
            df.rename(columns={"Item No.": "Item"},inplace=True)

        elif(i=="Item 10"):
            df.rename(columns={"Item 10": "Item"},inplace=True)
        if(i=="Quantity/ Unit"):
            df.rename(columns={"Quantity/ Unit": "Quantity"}, inplace=True)

        elif (i == "Qty"):
            df.rename(columns={"Qty": "Quantity"}, inplace=True)
        if (i == "Total value"):
            df.rename(columns={"Total value": "Total"}, inplace=True)


    item_list = df["Item"].tolist()[1:]
    qty_list = df["Quantity"].tolist()[1:]
    total_list = df["Total"].tolist()[1:]

    final_item.extend([x for x in item_list if str(x) != 'nan'])

    f.extend([x for x in qty_list if str(x) != 'nan'])

    t.extend([x for x in total_list if str(x) != 'nan'])

    final_qty = [str(i).replace("PC", "") for i in f]
    final_qty = [str(i).replace(",", "") for i in final_qty]
    final_qty = [float(x) for x in final_qty]

    final_total = [float(x) for x in t]

    for i in final_total:
        if i==250.0:
            final_total.remove(i)
        if i == 72.0:
            final_total.remove(i)



    if any((df.columns.duplicated()== True)) :
        df = df.loc[:, ~df.columns.duplicated()]

    if len([str(i) for i in data_list if str(i) == "nan"]) > 0 or any(
            [title_list[i] == data_list[i] for i in range(len(title_list))]):
        df = df[1:]




    df.to_csv(os.path.join(out_dir, filename), index=None, sep=',', mode='w')
    data = pd.read_csv('Output/' + filename)
    for i in data.columns:
        if(i=="nan"):
            data.drop(columns=["nan"], inplace=True)
    df=data



    df.to_csv(os.path.join(out_dir, filename), index=None, sep=',', mode='w')
    lst=df.columns
    databaseConnection(lst,table_name,filename)

def databaseConnection(lst,table_name,filename):
    removetable = str.maketrans(' /:.#()%-','_________')
    out_list = [s.translate(removetable) for s in lst]

    #out_list=out_list.replace("Order", "Ordre")
    for i in out_list:
        if(i=="Order"):
           k=out_list.index(i)
           i = i.replace("Order", "Ordre")
           out_list[k]=i


    createsqltable='CREATE OR REPLACE TABLE '+table_name+'('+' VARCHAR(250),'.join(out_list)+' VARCHAR(250))'

    cursor.execute(createsqltable)
    lst1 = ['%s' for i in out_list]
    lst1 = ','.join(lst1)
    csv_data = csv.reader(open('Output/' + filename,encoding='UTF-8'))
    count = 0

    for row in csv_data:
        if count>=1:

            cursor.execute('INSERT INTO ' + table_name +' VALUES('+lst1+')',row)

        count+=1
    mydb.commit()


def graph():
    global final_qty
    global final_total

    y_pos = np.arange(len(final_item))
    plt.figure(1)
    plt.subplot(311)
    plt.barh(y_pos, final_qty, align='center', alpha=0.5, height=0.8)
    plt.yticks(y_pos, final_item)
    plt.ylabel('Items')
    plt.xlabel('Quantity')
    plt.title('Quantity per Item')

    plt.subplot(313)
    plt.barh(y_pos, final_total, align='center', alpha=0.5, height=0.8)
    plt.yticks(y_pos, final_item)
    plt.ylabel('Items')
    plt.xlabel('Total Price')
    plt.title('Total Price per Item')
    plt.show()


try:
    if __name__ == "__main__":
        gui.application_window.mainloop()
except:
    print("Exception Occurred")